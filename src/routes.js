const express = require('express')

const routes = express.Router()

const authMiddleware = require('./app/middlewares/auth')

const controllers = require('./app/controllers')

routes.post('/users', controllers.UserController.store)

routes.post('/sessions', controllers.SessionController.store)

routes.use(authMiddleware)

/**
 * Post
 */
routes.get('/post', controllers.PostController.index)
routes.get('/post/:id', controllers.PostController.show)
routes.post('/post', controllers.PostController.store)
routes.put('/post/:id', controllers.PostController.update)
routes.delete('/post/:id', controllers.PostController.destroy)
routes.post('/post/:id', controllers.PostController.coment)

/**
 * Comment
 */
routes.get('/comment', controllers.CommentController.index)
routes.get('/comment/:id', controllers.CommentController.show)
routes.put('/comment/:id', controllers.CommentController.update)
routes.delete('/comment/:id', controllers.CommentController.destroy)
module.exports = routes
