const express = require('express')
const mongoose = require('mongoose')
const databseConfig = require('./config/database')
const cors = require('cors')

class App {
  constructor () {
    this.express = express()
    this.express.use(cors())
    this.isDev = process.env.NODE_ENV !== 'production'

    this.database()
    this.middlewares()
    this.routes()
  }

  database () {
    mongoose.connect(databseConfig.uri, {
      useCreateIndex: true,
      useNewUrlParser: true
    })
  }

  middlewares () {
    this.express.use(express.json())
  }

  routes () {
    this.express.use(require('./routes'))
  }
}

module.exports = new App().express
