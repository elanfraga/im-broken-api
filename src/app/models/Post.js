const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const Post = new mongoose.Schema({
  text: {
    type: String,
    required: true
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  comments: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Comment',
      require: false
    }
  ],
  createdAT: {
    type: Date,
    default: Date.now
  }
})

Post.plugin(mongoosePaginate)

module.exports = mongoose.model('Post', Post)
