const mongoose = require('mongoose')

const Comment = new mongoose.Schema({
  commentText: {
    type: String,
    require: true
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    require: true
  },
  createdAT: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('Comment', Comment)
