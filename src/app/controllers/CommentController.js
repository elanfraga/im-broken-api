const Comment = require('../models/Comment')

class CommentController {
  async index (req, res) {
    const comment = await Comment.find()

    return res.json(comment)
  }

  async show (req, res) {
    const comment = await Comment.findById(req.params.id)

    return res.json(comment)
  }

  async update (req, res) {
    console.log(req.params.id)
    console.log(req.body)
    const comment = await Comment.findByIdAndUpdate(req.params.id, req.body, {
      new: true
    })

    return res.json(comment)
  }

  async destroy (req, res) {
    await Comment.findByIdAndDelete(req.params.id)

    return res.send()
  }
}

module.exports = new CommentController()
