const Post = require('../models/Post')
const Comment = require('../models/Comment')

class PostController {
  async index (req, res) {
    const post = await Post.paginate(
      {},
      {
        page: req.query.page || 1,
        limit: req.query.itensPerPage || 20,
        populate: ['author', 'comments'],
        sort: '-createdAT'
      }
    )

    return res.json(post)
  }

  async show (req, res) {
    const post = await Post.findById(req.params.id).populate('comments')

    return res.json(post)
  }

  async store (req, res) {
    const post = await Post.create({ ...req.body, author: req.userId })

    return res.json(post)
  }

  async coment (req, res) {
    const post = await Post.findById(req.params.id)

    const comment = await Comment.create({
      ...req.body,
      post: post._id,
      author: req.userId
    })

    post.comments.push(comment)

    await post.save()

    return res.json(post)
  }

  async update (req, res) {
    const post = await Post.findByIdAndUpdate(req.params.id, req.body, {
      new: true
    })

    return res.json(post)
  }

  async destroy (req, res) {
    await Comment.remove({ post: req.params.id })
    await Post.findByIdAndDelete(req.params.id)

    return res.send()
  }
}

module.exports = new PostController()
